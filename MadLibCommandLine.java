import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.util.regex.Pattern;

public class MadLib{

	//Hey maybe uhhhh put in gui code :ok_hand:

	public static void main(String[] args){
		System.out.print("Please type a file name: ");
		Scanner inputScanner = new Scanner(System.in);
		String inFile = inputScanner.next();
		File file = new File(inFile);
		String fulllib = "";
		try{
			Scanner fileScanner = new Scanner(file).useDelimiter(Pattern.compile("_"));
			int i = 0;
			while(fileScanner.hasNext()){
				if(i % 2 == 0){
					fulllib += fileScanner.next();
				}else{
					System.out.print("Please type a " + fileScanner.next() + ": ");
					fulllib += inputScanner.next();
				}
				i++;
			}
		}catch(FileNotFoundException e){
			System.out.println("Couldn't find file.");
		}finally{
			System.out.println(fulllib);
		}
	}
}
