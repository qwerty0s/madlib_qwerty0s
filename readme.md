This java applet is a little gui that reads a file given with a plain text
format and gives the user a mad lib of it. Some requirements for input files:

	1. Words to be replaced may be replaced by any string, provided that
	they are bounded by underscores (such as _noun_)
		a. The applet will say "Please type a $s" where $s is what is
		bounded by underscores (in this case, "noun.")

	2. They must not start with something that is to be replaced.
		a. If you want it to, start with a space and then put it.
		(for example, rather than "[SOF]_noun_" you must type "[SOF] 
		_noun_.")

	3. If you need it to ask for the something, start your word type with
	a $ (for example, "_$noun_" it will ask for the noun)

	4. Users can be difficult. Some people will type profanities for every
	single input, and there is nothing you can do about it.
