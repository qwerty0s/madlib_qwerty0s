import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;

public class MadLib implements ActionListener{

	JFrame window = new JFrame("Mad Lib");
	JPanel board = new JPanel(new GridLayout(6,1));
	JLabel fileNameLabel = new JLabel("File Name:");
	JTextField fileName = new JTextField(25);
	JLabel requests = new JLabel("Word types will appear here");
	JTextField input = new JTextField(25);
	JPanel outPanel = new JPanel(new GridLayout(2,1));
	JTextArea outArea = new JTextArea("The output will appear here");
	JScrollPane scrollOut = new JScrollPane(outArea);
	JButton writeToFile = new JButton("Write to file...");
	JPanel fileWriting = new JPanel(new GridLayout(2,1));
	JPanel fileWritingButtons = new JPanel(new GridLayout(1,2));
	JPanel fileWritingButtonsTwo = new JPanel(new GridLayout(1,2));
	JTextField outFileName = new JTextField(25);
	JButton write = new JButton("Write");
	JButton restart = new JButton("Reset");
	JButton exit = new JButton("Exit");
	JButton exitTwo = new JButton("Exit");
	String fulllib = "";
	boolean wantInput = false;
	int i = 0;
	boolean complete = false;
	Scanner fileScanner;
	String inFileName;
	File file;
	String outputFileName;

	public MadLib(){
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setSize(300,600);
		board.setBorder(new EmptyBorder(5, 5, 5, 5));
		fileNameLabel.setHorizontalAlignment(JLabel.CENTER);
		requests.setHorizontalAlignment(JLabel.CENTER);
		outArea.setEditable(false);
		outArea.setLineWrap(true);
		outArea.setWrapStyleWord(true);
		input.setEditable(false);
		outFileName.setEditable(false);
		scrollOut.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollOut.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		fileName.addActionListener(this);
		input.addActionListener(this);
		writeToFile.addActionListener(this);
		outFileName.addActionListener(this);
		write.addActionListener(this);
		restart.addActionListener(this);
		exit.addActionListener(this);
		exitTwo.addActionListener(this);

		board.add(fileNameLabel);
		board.add(fileName);
		board.add(requests);
		board.add(input);
		board.add(scrollOut);
		board.add(outPanel);

		outPanel.add(restart);
		outPanel.add(fileWritingButtons);

		fileWriting.add(outFileName);
		fileWriting.add(fileWritingButtonsTwo);

		fileWritingButtons.add(exit);
		fileWritingButtons.add(writeToFile);

		fileWritingButtonsTwo.add(exitTwo);
		fileWritingButtonsTwo.add(write);

		window.setContentPane(board);
		window.setVisible(true);
	}
	public void actionPerformed(ActionEvent e){
		try{
			if (e.getSource() == fileName){
				fileName.setEditable(false);
				inFileName = fileName.getText();
				file = new File(inFileName);
				fileScanner = new Scanner(file).useDelimiter(Pattern.compile("_"));
				if(fileScanner.hasNext()){
					fulllib += fileScanner.next();
				}else{
					complete = true;
					outArea.setText(fulllib);
				}
				if(fileScanner.hasNext()){
					input.setEditable(true);
					String wordType = fileScanner.next();
					char firstLetter = wordType.toLowerCase().charAt(0);
					if(firstLetter == 'a' || firstLetter == 'e' || firstLetter == 'i' || firstLetter == 'o' || firstLetter == 'u'){
						requests.setText("Please type an " + wordType);
					}else if(firstLetter == '$'){
						requests.setText("Please type the " + wordType.substring(1));
					}else{
						requests.setText("Please type a " + wordType);
					}
				}else{
					complete = true;
					outArea.setText(fulllib);
				}
			}else if(e.getSource() == input){
				fulllib += input.getText();
				input.setText("");
				if(fileScanner.hasNext()){
					fulllib += fileScanner.next();
				}else{
					complete = true;
					input.setEditable(false);
					outArea.setText(fulllib);
				}
				if(fileScanner.hasNext()){
					String wordType = fileScanner.next();
					char firstLetter = wordType.toLowerCase().charAt(0);
					if(firstLetter == 'a' || firstLetter == 'e' || firstLetter == 'i' || firstLetter == 'o' || firstLetter == 'u'){
						requests.setText("Please type an " + wordType);
					}else if(firstLetter == '$'){
						requests.setText("Please type the " + wordType.substring(1));
					}else{
						requests.setText("Please type a " + wordType);
					}
				}else{
					complete = true;
					input.setEditable(false);
					outArea.setText(fulllib);
				}
			}else if(e.getSource() == writeToFile){
				if(complete){
					outPanel.remove(fileWritingButtons);
					outPanel.add(fileWriting);
					outPanel.validate();
					outFileName.setEditable(true);
					outFileName.setText("output.txt");
				}
			}else if(e.getSource() == outFileName){
				outputFileName = outFileName.getText();
				outFileName.setEditable(false);
			}else if(e.getSource() == write){
				if(outFileName.isEditable()){
					outputFileName = outFileName.getText();
					outFileName.setEditable(false);
				}
				if(outputFileName.equals("")){
					outputFileName = "Output.txt";
				}
				writeToFile(fulllib, outputFileName);
			}else if(e.getSource() == restart){
				init();
			}else if(e.getSource() == exit || e.getSource() == exitTwo){
				window.dispose();
			}
		}catch(IOException ioe){
			outArea.setText("Could not open " + inFileName + ".");
		}
	}
	public static void main(String[] args){
		if(args.length > 0){
			if(args[0].equals("-c")){
				textOnly();
			}
		}else{
			try{
				MadLib madlib = new MadLib();
				if(madlib.window.isDisplayable() == false){
					System.exit(0);
				}
			}catch(HeadlessException e){
				textOnly();
			}
		}

	}
	public static void textOnly(){
		System.out.print("Please type a file name: ");
		Scanner inputScanner = new Scanner(System.in).useDelimiter(Pattern.compile("\n"));
		String inFileName = inputScanner.next().trim();
		File inputFile = new File(inFileName);
		String fulllib = "";
		try{
			Scanner fileScanner = new Scanner(inputFile).useDelimiter(Pattern.compile("_"));
			int i = 0;
			while(fileScanner.hasNext()){
				if(i % 2 == 0){
					fulllib += fileScanner.next();
				}else{
					String wordType = fileScanner.next();
					char firstLetter = wordType.toLowerCase().charAt(0);
					if(firstLetter == 'a' || firstLetter == 'e' || firstLetter == 'i' || firstLetter == 'o' || firstLetter == 'u'){
						System.out.print("Please type an " + wordType + ": ");
					}else if(firstLetter == '$'){
						String realWordType = wordType.substring(1);
						System.out.print("Please type the " + realWordType + ": ");
					}else{
						System.out.print("Please type a " + wordType + ": ");
					}
					fulllib += inputScanner.next();
				}
				i++;
			}
		}catch(FileNotFoundException exception){
			System.out.println("Couldn't find file.");
		}finally{
			System.out.println(fulllib);
		}
	}
	public void writeToFile(String outText, String outFile){
		try{
			File outputFile = new File(outFile);
			FileOutputStream output = new FileOutputStream(outputFile, false);
			byte[] outTextToBytes = outText.getBytes();
			output.write(outTextToBytes);
		}catch(FileNotFoundException e){
			outArea.setText("Could not find " + outFile + ".");
		}catch(IOException e){
			outArea.setText("Error writing to file.");
		}
	}
	public void init(){
		window.dispose();
		JFrame window = new JFrame("Mad Lib");
		JPanel board = new JPanel(new GridLayout(6,1));
		JLabel fileNameLabel = new JLabel("File Name:");
		JTextField fileName = new JTextField(25);
		JLabel requests = new JLabel("Word types will appear here");
		JTextField input = new JTextField(25);
		JPanel outPanel = new JPanel(new GridLayout(2,1));
		JTextArea outArea = new JTextArea("The output will appear here");
		JScrollPane scrollOut = new JScrollPane(outArea);
		JButton writeToFile = new JButton("Write to file...");
		JPanel fileWriting = new JPanel(new GridLayout(2,1));
		JPanel fileWritingButtons = new JPanel(new GridLayout(1,2));
		JPanel fileWritingButtonsTwo = new JPanel(new GridLayout(1,2));
		JTextField outFileName = new JTextField(25);
		JButton write = new JButton("Write");
		JButton restart = new JButton("Reset");
		JButton exit = new JButton("Exit");
		JButton exitTwo = new JButton("Exit");
		String fulllib = "";
		boolean wantInput = false;
		int i = 0;
		boolean complete = false;
		Scanner fileScanner;
		String inFileName;
		File file;
		String outputFileName;
		MadLib madlib = new MadLib();
	}
}
